extends KinematicBody

var InventoryItem = preload("res://scenes/inventoryitem.tscn")
var WorldItemClass = preload("res://scenes/worlditem.gd")

var default_hand_texture = preload("res://sprites/hand_open.png")

var engine = null	# the adventure engine handler we are to call when we interact with the world

signal item_gained		# (this, InventoryItem)
signal examined_world_item	# (self, WorldItem)

export var speed = 300
export var fall_acceleration = 75
export var jump_impulse = 20

var velocity = Vector3.ZERO

var _rotation_acc: Vector2 = Vector2.ZERO

onready var camera = $Rotato/Camera
var _lookit = false
var _lookit_from: Vector3
var _lookit_to : Vector3
var _lookit_world_target = null
var _lookit_world_normal: Vector3
var lookit_item = null

var inventory = []
var held_item = null

var state = GameDefs.PlayerState.IDLE

const MAX_ROTATION = 2 * PI
const MAX_VERTICAL_ROTATION = PI * 9 / 20
const TERMINAL_VELOCITY = -1000

const LOOKIT_LENGTH = 5


func _ready():
	$Hand.visible = false

func set_engine(engine):
	self.engine = engine


func _physics_process(delta):
	var direction: Vector3 = Vector3.ZERO
	var speed_multiplier = 1
	
	# check input
	if state == GameDefs.PlayerState.IDLE:
		if Input.is_action_pressed("c_right"):
			direction.x += 1
		if Input.is_action_pressed("c_left"):
			direction.x -= 1
		if Input.is_action_pressed("c_back"):
			direction.z += 1
		if Input.is_action_pressed("c_forward"):
			direction.z -= 1
		if Input.is_action_pressed("c_run"):
			speed_multiplier = 2
	
		if direction != Vector3.ZERO:
			direction = direction.normalized()
	
		if is_on_floor() and Input.is_action_just_pressed("c_jump"):
			velocity.y += jump_impulse
	
	# max fall speed
	if velocity.y < TERMINAL_VELOCITY:
		velocity.y = TERMINAL_VELOCITY
	
	# work out the global velocity
	velocity = Vector3(0, velocity.y - (fall_acceleration * delta), 0)
	velocity += get_global_transform().basis.x * direction.x * speed * delta * speed_multiplier
	velocity += get_global_transform().basis.z * direction.z * speed * delta * speed_multiplier
	
	# move
	velocity = move_and_slide(velocity, Vector3.UP)


func update_world_cursor():
	queue_lookit()
	do_raycast()
	
	# draw world cursor
	if _lookit_world_target:
		$Hand.transform.origin = to_local(_lookit_world_target)
#		if not is_equal_approx(1.0, abs(_lookit_world_normal.dot(Vector3.UP))):
		if abs(_lookit_world_normal.dot(Vector3.UP)) <= 0.990:
			# we are on a non-horizontal surface
			# draw the hand rotated to match the surface it is on
			$Hand.look_at(_lookit_world_target + _lookit_world_normal, Vector3.UP)
		else:
			# we are on a horizontal surface (ceiling / floor).
			# draw the hand with the fingers pointing away from the player (-get_transform().basis.z)
			$Hand.look_at(_lookit_world_target + _lookit_world_normal, -get_transform().basis.z)
		$Hand.visible = true
	else:
		$Hand.visible = false


func _process(delta):
	if state == GameDefs.PlayerState.IDLE:
		update_world_cursor()


func _unhandled_input(event: InputEvent):
	if state != GameDefs.PlayerState.IDLE:
		# we only handle move / look actions
		return
		
	if event is InputEventMouseButton and event.pressed:
		if held_item:
			print("Player world item click")
			get_tree().set_input_as_handled()
			if lookit_item != null:
				interact_world_item(lookit_item)
		elif event.button_index == GameDefs.BUTTON_LEFT:
			print("Player world left click")
			get_tree().set_input_as_handled()
			if lookit_item != null:
				examine_world_item(lookit_item)
		elif event.button_index == GameDefs.BUTTON_RIGHT:
			print("Player world right click")
			get_tree().set_input_as_handled()
			if lookit_item != null:
				interact_world_item(lookit_item)
		
	elif event is InputEventMouseMotion:
		# mouselook
		get_tree().set_input_as_handled()
		
		# inspired by transform tut
		# https://docs.godotengine.org/en/stable/tutorials/3d/using_transforms.html#doc-using-transforms
		_rotation_acc.x -= (event.relative.x * GameDefs.mouse_sensitivity)
		_rotation_acc.y += (event.relative.y * GameDefs.mouse_sensitivity * GameDefs.mouse_uninvert_factor)
		
		# constrain our rotation accumulators
		while _rotation_acc.x > MAX_ROTATION:
			_rotation_acc.x -= MAX_ROTATION
		while _rotation_acc.x < -MAX_ROTATION:
			_rotation_acc.x += MAX_ROTATION
		
		if _rotation_acc.y > MAX_VERTICAL_ROTATION:
			_rotation_acc.y = MAX_VERTICAL_ROTATION
		if _rotation_acc.y < -MAX_VERTICAL_ROTATION:
			_rotation_acc.y = -MAX_VERTICAL_ROTATION
		
		# reset rotations
		self.transform.basis = Basis()
		$Rotato.transform.basis = Basis()
		
		# re-apply the rotations
		self.rotate_object_local(Vector3(0, 1, 0), _rotation_acc.x) # first rotate around the Y
		$Rotato.rotate_object_local(Vector3(1, 0, 0), _rotation_acc.y) # then rotate around the X (waist)


func queue_lookit(pos = null):
	"Inform the physics system we want to do a lookit"

	# default to middle of view
	if pos == null:
		pos = get_viewport().get_visible_rect().size / 2
	
	_lookit_from = camera.project_ray_origin(pos)
	_lookit_to = _lookit_from + camera.project_ray_normal(pos) * LOOKIT_LENGTH
	_lookit = true


func do_raycast():
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(_lookit_from, _lookit_to, [self], collision_mask & (0x7fffffff ^ 0b10), true, true)
	if result:
		_lookit_world_target = result.position + (0.01 * result.normal)
		_lookit_world_normal = result.normal
		lookit_item = result.collider
	else:
		_lookit_world_target = null
		lookit_item = null


func examine_world_item(item):
	"player asked for description of a world item"
	if not item is WorldItemClass:
		print("Can't examine '%s'" % [str(item)])
	else:
		print("Examining '%s' (#%s %s): %s" % [item.display_name, item.unique_name, str(item), item.description])
		engine.examine_world_item(self, item)

func hold_item(item):
	"We are now holding a new item in our hand"
	if not item:
		# unhold our item
		print("We are now holding nothing")
		held_item = null
		$Hand.texture = default_hand_texture
		return
	
	print("We are now holding %s" % [item])
	held_item = item
	$Hand.texture = item.texture_normal


func interact_world_item(item):
	"player tried to interact with a world item"
	if not item is WorldItemClass:
		print("Can't interact with '%s'" % [str(item)])
	else:
		engine.interact_world_item(self, item)


# access functions that our engine uses
func gain_item(item):
	"add an item to our inventory"
	print("Picking up '%s' (#%s %s)" % [item.display_name, item.unique_name, str(item)])
	var new_item = InventoryItem.instance()
	new_item.from_world_item(item)
	inventory.append(new_item)
	emit_signal("item_gained", self, new_item)
	
