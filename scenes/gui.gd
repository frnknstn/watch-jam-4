extends Control

signal item_selected	# (item)

onready var selected_item_material = preload("res://materials/selected_item.tres")

onready var item_bar = $VBoxContainer/ScrollContainer/ItemBar

onready var top_text_container = $VBoxContainer/TopTextContainer
onready var top_text_header = $VBoxContainer/TopTextContainer/VBoxContainer/HBoxContainer/TopTextHeader
onready var top_text_subheader = $VBoxContainer/TopTextContainer/VBoxContainer/HBoxContainer/TopTextSubheader
onready var top_text_body = $VBoxContainer/TopTextContainer/VBoxContainer/MarginContainer/TopTextBody
onready var top_text_hide_timer = $VBoxContainer/TopTextContainer/HideTimer

onready var bottom_text_container = $BottomTextContainer
onready var bottom_text_heading = $BottomTextContainer/MarginContainer2/VBoxContainer/BottomTextHeading
onready var bottom_text_body = $BottomTextContainer/MarginContainer2/VBoxContainer/MarginContainer/BottomTextBody

var selected_item = null
var examining_item = null
var hovering_item = null

var inventory_owner = null

enum TopTextVisModes {
	HIDDEN,
	INV_HOVER,
	INV_EXAMINE,	
}


func _ready():
	_set_top_text_vis_mode(TopTextVisModes.HIDDEN)
	bottom_text_container.visible = false
	$InventoryBackdrop.visible = false


# we control the bottom text more directly, as it is modal
func show_bottom_text(title:String, message:String):
	"display a message in the bottom text box"
	bottom_text_container.visible = true
	bottom_text_heading.text = title
	bottom_text_body.text = message
	
	# unclutter our GUI
	top_text_hide_timer.stop()
	_set_top_text_vis_mode(TopTextVisModes.HIDDEN)
	examining_item = null
	hovering_item = null
	
	

func hide_bottom_text():
	bottom_text_container.visible = false
	
	
func show_top_text(title:String, message:String, context:String = ""):
	"display a message in the top text box"
	top_text_header.text = title
	top_text_body.text = message
	top_text_subheader.text = context
	top_text_hide_timer.stop()

func examine_inventory_item(item, context_message:String = ""):
	"show an item's inventory description text, with a subheading providing extra context"
	examining_item = item
	if context_message != "":
		context_message = "<" + context_message + ">"
	show_top_text(item.display_name, item.inv_description, context_message)
	_set_top_text_vis_mode(TopTextVisModes.INV_EXAMINE)
	
	# hide the box after a timeout
	var hide_time = len(item.inv_description) / GameDefs.READ_SPEED
	print([hide_time, GameDefs.MIN_ITEM_EXAMINE_TIME])
	top_text_hide_timer.start(max(hide_time, GameDefs.MIN_ITEM_EXAMINE_TIME))
	
func examine_world_item(item, context_message:String = ""):
	"show an item's inventory description text, with a subheading providing extra context"
	examining_item = item
	if context_message != "":
		context_message = "<" + context_message + ">"
	show_top_text(item.display_name, item.description, context_message)
	_set_top_text_vis_mode(TopTextVisModes.INV_EXAMINE)
	
	# hide the box after a timeout
	var hide_time = len(item.inv_description) / GameDefs.READ_SPEED
	print([hide_time, GameDefs.MIN_ITEM_EXAMINE_TIME])
	top_text_hide_timer.start(max(hide_time, GameDefs.MIN_ITEM_EXAMINE_TIME))

func select_item(item):
	if not item or item == selected_item:
		# deselect and move on
		print("Deselected %s" % selected_item)
		selected_item.material = null
		selected_item = null
	else:
		print("Selected %s" % [item])
		if selected_item != null:
			print("Deselected %s" % selected_item)
			selected_item.material = null
		selected_item = item
		selected_item.material = selected_item_material
		emit_signal("item_selected", item)
		
	if inventory_owner:
		inventory_owner.hold_item(selected_item)


func set_inventory_vis(vis:bool):
	$InventoryBackdrop.visible = vis
	

func _on_Player_item_gained(player, item):
	"a player has gained an inventory item"
	item_bar.add_child(item)
	item.connect("selected", self, "_on_InventoryItem_selected")
	item.connect("hover", self, "_on_InventoryItem_hover")
	item.connect("stop_hover", self, "_on_InventoryItem_stop_hover")
	item.connect("deselected", self, "_on_InventoryItem_deselected")


func _on_InventoryItem_selected(item):
	select_item(item)

func _on_InventoryItem_deselected(item):
	"a held item is explicitly no longer held"
	assert(selected_item == item)
	select_item(null)

func _on_HideTimer_timeout():
	examining_item = null
	if hovering_item:
		_set_top_text_vis_mode(TopTextVisModes.INV_HOVER)
	else:
		_set_top_text_vis_mode(TopTextVisModes.HIDDEN)

func _on_InventoryItem_hover(item):
	"mouse is hovering over item in inventory, give a hint"
	top_text_hide_timer.stop()
	hovering_item = item
	examine_inventory_item(item, "examining inventory item")
	_set_top_text_vis_mode(TopTextVisModes.INV_HOVER)

func _on_InventoryItem_stop_hover(item):
	"mouse is no longer hovering over item in inventory"
	# hide the text unless we are busy examining an item
	hovering_item = null
	if top_text_hide_timer.is_stopped():
		_set_top_text_vis_mode(TopTextVisModes.HIDDEN)


func _set_top_text_vis_mode(mode):
	match mode:
		TopTextVisModes.HIDDEN:
			top_text_container.visible = false
			top_text_subheader.visible = false
			top_text_body.visible = false
		TopTextVisModes.INV_EXAMINE:
			top_text_container.visible = true
			top_text_subheader.visible = true
			top_text_body.visible = true
		TopTextVisModes.INV_HOVER:
			top_text_container.visible = true
			top_text_subheader.visible = true
			top_text_body.visible = true


func toggle_help(vis = null):
	var help = $HelpControl
	if vis != null:
		help.visible = vis
	else:
		help.visible = ! help.visible
		
		
