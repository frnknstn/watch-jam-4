extends StaticBody

var Cutscene = preload("res://cutscene.gd")


# standard world item exports
export(String) var unique_name   # unique string identifier
export(String) var display_name   # display name for the item in your inventory
export(String) var description    # text displayed when looking at object in the world
export(GameDefs.WorldItemType) var item_type	# category of item

# portable item exports
export(String) var inv_description    # text displayed when looking at object in your inventory
export(Texture) var inv_texture   # texture used in the inventory

# interactive item exports
export(String) var action_description
export(String) var action_key
export(NodePath) var action_target


func start_action(args = null):
	"""Begin an action, returning a Cutscene for the game to play"""
	print(self, self.display_name, self.action_description)
	var target_item = get_node(action_target)
	var cutscene = Cutscene.new([[display_name, action_description]], funcref(target_item, "do_action"))
	return cutscene
	


func do_action(args = null):
	"""Overloadable method of interactive items and doors. 
	
	This method is called when an Action targets this object.
	"""
	# TODO: remove this test code
	self.transform.origin.y += 2.5
	pass
