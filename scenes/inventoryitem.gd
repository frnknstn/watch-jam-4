extends TextureButton

signal selected		# (self)
signal deselected	# (self)
signal destroyed	# (self)
signal hover		# (self)
signal stop_hover		# (self)

export(String) var unique_name   # unique string identifier
export(String) var display_name   # display name for the item in your inventory
export(String) var inv_description    # text displayed when looking at object in your inventory


var last_button = 0


func from_world_item(item):
	"copy some details from a world item"
	unique_name = item.unique_name
	display_name = item.display_name
	inv_description = item.inv_description
	texture_normal = item.inv_texture


func destroy():
	emit_signal("destroyed", self)
	queue_free()


func unhold():
	"Player is putting us down"
	emit_signal("deselected", self)


func _on_InventoryItem_pressed():
	emit_signal("selected", self)


func _on_InventoryItem_mouse_entered():
	emit_signal("hover", self)


func _on_InventoryItem_mouse_exited():
	emit_signal("stop_hover", self)


func _on_InventoryItem_gui_input(event):
	# track the last button to affect us so we know if we were left or right clicked
	if event is InputEventMouseButton:
		last_button = event.button_index
