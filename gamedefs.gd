extends Node


const BUTTON_LEFT = 1
const BUTTON_RIGHT = 2

const READ_SPEED = 15.0
const MIN_ITEM_EXAMINE_TIME = 3.0


enum PlayerState {
	IDLE,		# walking around
	CUTSCENE,	# in a talk box or modal animation
	INVENTORY,	# interacting with inventory
}


enum WorldItemType {
	PORTABLE,		# can get picked up and placed in inventory
	INTERACTIVE,	# does something when you interact with it (optionally requires a key)
	GATE,			# changes state when signaled by an interactive item
}


var mouse_uninvert_factor = 1
var mouse_sensitivity = 0.006

