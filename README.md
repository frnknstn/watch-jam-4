## Watch + Jam 4 #

3D game for Watch + Jam 4

### Feature wishlist #

* draw hand on world
* inventory
  * item pickup
  * use item with world
  * combine
* state
  * world state
  * room state
* conversations
  * conversations depend on world / room state
* external scripts

## Test Puzzle #

Dev test puzzle 1: "Use the shiny textbook on the moondial to open the door"

objects:
* Shiny textbook - portable
* Moondial - interactive
* Door - gate

## Concepts #

### Actions #

Actions have 3 main bits of context:

* action_description - Once triggered, show this text
* action_key - If present, player must be holding this item to trigger the action
* action_target - If present, find the target item and call its action function

## Assets and attribution

* OpenGameArt.org
	* https://opengameart.org/content/mouse-hand-icons
		* CC0
		* Cough-E
		* HandIcons.png
  * https://opengameart.org/content/book-icon-photoshop-template
    * CC0
    * smirlianos
    * BookIconTemplate.psd
  * https://opengameart.org/content/balloon-rising
    * CC0
    * emacsabino
    * balao_anime.zip
  * https://opengameart.org/content/50-rpg-sound-effects
    * CC0
    * Kenney.nl
    * RPGsounds_Kenney.zip



* CC0 Textures
  * https://ambientcg.com/view?id=Rock029

