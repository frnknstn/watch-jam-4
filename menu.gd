extends CanvasLayer

onready var StartButton = $MarginContainer/VBoxContainer/StartButton
onready var SkipButton = $MarginContainer/VBoxContainer/SkipButton


func _ready():
	$Scroller.visible = false
	$MarginContainer.visible = false
	$Title.visible = false
	$Byline.visible = false
	$StartAnimation.play("start")

func _on_StartButton_pressed():
	$Scroller.visible = true
	$IntroAnimation.play("intro")

func start():
	StartButton.visible = false
	SkipButton.text = "LOADING..."
	yield(get_tree().create_timer(0.05), "timeout")
	get_tree().change_scene("res://main.tscn")

func _on_SkipButton_pressed():
	start()




