extends Node

var InventoryItem = preload("res://scenes/inventoryitem.tscn")
var PlayerState = GameDefs.PlayerState

onready var gui = $GUI

var state = PlayerState.IDLE
var active_cutscene: Cutscene


func _process(delta):
	OS.set_window_title("Watch + Jam 4 - " + str(Engine.get_frames_per_second()) + " fps")


func _ready():
	randomize()
	gui.toggle_help(false)
	
	# link our player to the GUI
	$Player.connect("item_gained", gui, "_on_Player_item_gained")
	gui.inventory_owner = $Player
	gui.connect("item_selected", self, "_on_GUI_item_selected")
	
	# link our player to the adventure engine
	$Player.set_engine(self)
	set_state(PlayerState.IDLE)

	
func _unhandled_input(event):
	if event.is_action("ui_cancel"):
		print("exiting game")
		get_tree().set_input_as_handled()
		get_tree().quit()
	elif event.is_action("ui_help") and event.pressed:
		get_tree().set_input_as_handled()
		gui.toggle_help()
	elif event.is_action("ui_uninvert") and event.pressed:
		get_tree().set_input_as_handled()
		GameDefs.mouse_uninvert_factor *= -1
	
	elif state == PlayerState.IDLE or state == PlayerState.INVENTORY:
		if event.is_action("g_inventory") and event.pressed:
			toggle_inventory_mode()
	
	elif state == PlayerState.CUTSCENE:
		# actions to advance text
		if (event is InputEventMouseButton and event.pressed):
			get_tree().set_input_as_handled()
			advance_active_cutscene()
		elif (event.is_action("g_inventory") and event.pressed):
			get_tree().set_input_as_handled()
			if not advance_active_cutscene():
				# cutscene ended, open the inventory
#				toggle_inventory_mode()
				pass

func toggle_inventory_mode():
	"toggle mouse inventory mode"
	if state == PlayerState.IDLE:
		# change to inventory mode
		get_tree().set_input_as_handled()
		set_state(PlayerState.INVENTORY)
		gui.set_inventory_vis(true)
	elif state == PlayerState.INVENTORY:
		# change to walkies mode
		get_tree().set_input_as_handled()
		set_state(GameDefs.PlayerState.IDLE)
		gui.set_inventory_vis(false)

func _on_GUI_item_selected(item):
	# release the inventory mode
	if state == PlayerState.INVENTORY:
		get_tree().set_input_as_handled()
		set_state(PlayerState.IDLE)


func examine_world_item(player, item):
	"Player is telling us they have examined a world item"
	gui.examine_world_item(item, "examining")


func interact_world_item(player, item):
	"Player is telling us they have interacted with a world item"
	match item.item_type:
		GameDefs.WorldItemType.PORTABLE:
			if player.held_item:
				handle_player_interaction(player, item)
			else:
				# pick it up
				player.gain_item(item)
				item.queue_free()
				# pickup messages
				print("%s has gained an inventory item '%s'" % [player, item])
				bottom_text("Picked up %s" % [item.display_name], item.inv_description)
		GameDefs.WorldItemType.INTERACTIVE:
			# interact with it
			handle_player_interaction(player, item)
		GameDefs.WorldItemType.GATE:
			# This does nothing for now
			print("can't interact with gates directly yet")
			handle_player_interaction(player, item)


func handle_player_interaction(player, item):
	"A player has interacted with a INTERACTIVE world item"

	if player.held_item:
		var held_item = player.held_item
		var held_item_id = held_item.unique_name
		
		if item.action_key and held_item_id == item.action_key:
			bottom_text(item.display_name, item.action_description)
			
			# do the thing
			var cutscene = item.start_action()
			if cutscene:
				start_cutscene(cutscene)
		else:
			# wrong item
			bottom_text(item.display_name, 
				"You slowly rub the %s against the %s for a while, until you start to feel slightly silly." \
				% [held_item.display_name, item.display_name])
		
		# we tried to use a thing, unselect
		held_item.unhold()
		
	elif item.action_key:
		# empty-handed
		bottom_text(item.display_name,
			"You prod at the %s with your foot, but nothing happens." % [item.display_name])
	else:
		# do the action
		# not yet implemented
		bottom_text(item.display_name,
			"You prod at the %s with your foot, but nothing happens." % [item.display_name])


func set_state(new_state):
	"Set the gamestate and the player state"
	print("Set gamestate to %d" % [new_state])
	state = new_state
	if state == PlayerState.IDLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	elif state == PlayerState.INVENTORY:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	elif state == PlayerState.CUTSCENE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	if $Player:
		$Player.state = state


func start_cutscene(cutscene):
	"Kick off a cutscene"
	set_state(GameDefs.PlayerState.CUTSCENE)
	active_cutscene = cutscene
	advance_active_cutscene()

func advance_active_cutscene():
	"Move on to the next scene. Returns false if the cutscene is over."
	if active_cutscene.is_over():
		active_cutscene.finale()
		set_state(PlayerState.IDLE)
		gui.hide_bottom_text()
		active_cutscene = null
		return false
	
	var scene = active_cutscene.next()
	gui.show_bottom_text(scene[0], scene[1])
	return true
	

func bottom_text(title:String, body:String):
	"Show a simple scene in the bottom text box"
	start_cutscene(Cutscene.new([[title, body]]))
