extends Resource
class_name Cutscene

signal cutscene_over

var _script
var _script_page = 0
var _callback_complete: FuncRef

var _over = false

func _init(script, callback_complete: FuncRef = null):
	self._script = script
	self._callback_complete = callback_complete

func next():
	"Return the next scene"
	var page = _script[_script_page]
	
	# turn the page
	_script_page += 1
	if _script_page >= len(_script):
		_over = true
	
	return(page)

func finale():
	"finish up"
	assert(_over)
	if _callback_complete:
		_callback_complete.call_func()
	emit_signal("cutscene_over")

func is_over():
	return _over
